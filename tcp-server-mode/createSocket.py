import socket
import struct


class SocketCommandInterface(object):

    def __init__(self, mode='wifi'):
        self.mode = mode
        self.conn = None
        if self.mode == 'bluetooth':
            self.soc = socket.socket(socket.AF_BLUETOOTH, socket.SOCK_STREAM, socket.BTPROTO_RFCOMM)
        else:
            self.soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.soc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    def __del__(self):
        self.close()

    def open(self, host, port, timeout=1):
        if self.mode == 'bluetooth':
            self.soc.connect((host, port))
        else:
            self.soc.bind((host, port))
            self.soc.listen(1)

    def write(self, opcode, data=None):
        msg = (opcode,)
        if data:
            msg += data
        # print(">> write:", msg)
        try:
            self.conn.send(struct.pack('B' * len(msg), *msg))
        except BrokenPipeError:
            pass

    def read(self, num_bytes):
        data = self.conn.recv(num_bytes)
        return data

    def close(self):
        self.soc.close()

import create2api
import time
from config import *


class Robot:

    def __init__(self):
        self.bot = create2api.Create2(HOST, PORT)

    def init(self):
        self.bot.start()
        self.bot.safe()
        time.sleep(1)

    def move(self, direction):
        self._move_forward() if direction == 'front' else self._move_back()

    def rotate(self, direction):
        self._rotate_right() if direction == 'right' else self._rotate_left()

    def stop(self):
        self.bot.digit_led_ascii(TEXT_STOP)
        self.bot.drive_direct(0, 0)

    def terminate(self):
        self.bot.terminate()

    def _move_forward(self):
        self.bot.digit_led_ascii(TEXT_FRONT)
        self.bot.drive_direct(SPEED, SPEED)

    def _move_back(self):
        self.bot.digit_led_ascii(TEXT_BACK)
        self.bot.drive_direct(-SPEED, -SPEED)

    def _rotate_right(self):
        self.bot.digit_led_ascii(TEXT_RIGHT)
        self.bot.drive_direct(-SPEED, SPEED)

    def _rotate_left(self):
        self.bot.digit_led_ascii(TEXT_LEFT)
        self.bot.drive_direct(SPEED, -SPEED)


if __name__ == '__main__':
    bot = Robot()
    bot.init()
    time.sleep(3)
    bot.rotate('right')
    time.sleep(5)
    bot.rotate('left')
    time.sleep(5)
    bot.stop()
    time.sleep(1)

import socket
import struct
from config import *


class SocketCommandInterface(object):

    def __init__(self):
        self.soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.soc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.soc.settimeout(TIMEOUT)

    def __del__(self):
        self.close()

    def open(self, host, port):
        self.soc.connect((host, port))

    def write(self, opcode, data=None):
        msg = (opcode,)
        if data:
            msg += data
        # print(">> write:", msg)
        self.soc.send(struct.pack('B' * len(msg), *msg))

    def close(self):
        self.soc.close()

#include <WiFi.h>
#include <SoftwareSerial.h>


const char* ssid = "ssid";
const char* password = "password";

/* create a server and listen on port 47295 */
WiFiServer server(47295);

int rxPin=16;
int txPin=17;
SoftwareSerial roomba;

int brcPin = 2;

void wakeUp(void) {
  int i;
  for(i = 0; i < 3; i++) {
    digitalWrite(brcPin, HIGH);
    delay(250);
    digitalWrite(brcPin, LOW);
    delay(250);
  }
  digitalWrite(brcPin, HIGH);
  delay(1000);
}

void setup() {
  pinMode(brcPin, OUTPUT);

  Serial.begin(19200);
  Serial.print("Connecting to ");
  Serial.println(ssid);

  /* connecting to WiFi */
  WiFi.begin(ssid, password);
  /*wait until ESP32 connect to WiFi*/
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected with IP address: ");
  Serial.println(WiFi.localIP());

  /* start Server */
  server.begin();

  roomba.begin(19200, SWSERIAL_8N1, rxPin, txPin, false, 95);

  delay(1000);
  wakeUp();
}

void loop() {
  /* listen for client */
  WiFiClient client = server.available();
  delay(5);
  if (client) {
    Serial.println("new client");
    /* check client is connected */
    while (client.connected()) {
      delay(5);
      if (client.available()) {
        roomba.write(client.read());
      }
    }
  }
}

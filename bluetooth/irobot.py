import create2api
import time
import sys


def get_sensors():
    sensor = bot.get_sensors()
    print("[L ] [LF] [LC] [CR] [RF] [ R]")
    print(f"{sensor.light_bumper_left:4} {sensor.light_bumper_front_left:4} {sensor.light_bumper_center_left:4} {sensor.light_bumper_center_right:4} {sensor.light_bumper_front_right:4} {sensor.light_bumper_right:4}")

def change_speed():
    global speed
    speed = int(input('SPEED? '))

def move_forward():
    print('>> RIGHT: %i    LEFT: %i' % (speed, speed))
    bot.drive_direct(speed, speed)


def move_back():
    print('>> RIGHT: %i    LEFT: %i' % (-speed, -speed))
    bot.drive_direct(-speed, -speed)


def rotate_right():
    print('>> RIGHT: %i    LEFT: %i' % (-speed, speed))
    bot.drive_direct(-speed, speed)


def rotate_left():
    print('>> RIGHT: %i    LEFT: %i' % (speed, -speed))
    bot.drive_direct(speed, -speed)


def stop_moving():
    print('>> RIGHT: %i    LEFT: %i' % (0, 0))
    bot.drive_direct(0, 0)


def shut_down():
    print('shutting down ... bye')
    bot.drive_stop()
    time.sleep(0.1)
    exit()


def main():
    bot.start()
    time.sleep(6)
    bot.safe()
    while True:
        cmd = input('iRobot> ')
        if cmd == '':
            continue
        elif cmd == 'cs':
            change_speed()
            continue
        led_ascii = led.get(cmd, 'bye')
        bot.digit_led_ascii(led_ascii)
        if cmd == 'w':
            move_forward()
        elif cmd == 's':
            move_back()
        elif cmd == 'd':
            rotate_right()
        elif cmd == 'a':
            rotate_left()
        elif cmd == 'f':
            stop_moving()
        elif cmd == 'sensors':
            get_sensors()
        else:
            shut_down()


print('Please wait ...')

led = {
    'w': 'forw',
    's': 'back',
    'd': 'rite',
    'a': 'left',
    'f': 'stop'
}

mac_address = sys.argv[1]
port = int(sys.argv[2])

bot = create2api.Create2(mac_address, port)
speed = 40 # 0 - 500 mm/s

main()

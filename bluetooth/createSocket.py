import socket
import struct


class SocketCommandInterface(object):

    def __init__(self):
        self.soc = socket.socket(socket.AF_BLUETOOTH, socket.SOCK_STREAM, socket.BTPROTO_RFCOMM)

    def __del__(self):
        self.close()

    def open(self, mac_address, port, timeout=1):
        self.soc.connect((mac_address, port))

    def write(self, opcode, data=None):
        msg = (opcode,)
        if data:
            msg += data
        # print(">> write:", msg)
        self.soc.send(struct.pack('B' * len(msg), *msg))

    def read(self, num_bytes):
        data = self.soc.recv(num_bytes)
        return data

    def close(self):
        self.soc.close()
